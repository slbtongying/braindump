import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Window 2.0

import braindump 1.0
import braindump.Views 1.0

ApplicationWindow
{
  visible: true
  width: 300
  height: 240
  
  visibility: ApplicationWindow.Maximized

  DatabaseInterface
  {
    id: databaseInterface
  }
  Rectangle
  {
    anchors.fill: parent
    color: "white"
    NodesView
    {
      anchors.fill: parent
      databaseInterface: databaseInterface
      node: databaseInterface.root
    }
  }
}
