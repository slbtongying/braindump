import QtQuick 2.0
import QtQuick.Controls 2.3

import braindump 1.0
import braindump.db 1.0 as BDB


Column
{
  id: root
  property alias node: textArea.node
  ToolBar
  {
    id: toolBar
    visible: textArea.activeFocus

    Action {
      id: boldAction
      text: "&Bold"
      icon.name: "format-text-bold"
      onTriggered: document.bold = !document.bold
      checkable: true
      checked: document.bold
    }
    Action {
      id: italicAction
      text: "&Italic"
      icon.name: "format-text-italic"
      onTriggered: document.italic = !document.italic
      checkable: true
      checked: document.italic
    }
    Action {
      id: underlineAction
      text: "&Underline"
      icon.name: "format-text-underline"
      onTriggered: document.underline = !document.underline
      checkable: true
      checked: document.underline
    }
    Action {
      id: alignLeftAction
      text: "Align left"
      icon.name: "format-justify-left"
      checkable: true
      checked: document.alignment == Qt.AlignLeft
      onTriggered: document.alignment = Qt.AlignLeft
    }
    Action {
      id: alignCenterAction
      text: "Align center"
      icon.name: "format-justify-center"
      checkable: true
      checked: document.alignment == Qt.AlignHCenter
      onTriggered: document.alignment = Qt.AlignHCenter
    }
    Action {
      id: alignRightAction
      text: "Align right"
      icon.name: "format-justify-right"
      checkable: true
      checked: document.alignment == Qt.AlignRight
      onTriggered: document.alignment = Qt.AlignRight
    }
    Action {
      id: alignJustifyAction
      text: "Align justify"
      icon.name: "format-justify-fill"
      checkable: true
      checked: document.alignment == Qt.AlignJustify
      onTriggered: document.alignment = Qt.AlignJustify
    }
    Row
    {
      ToolButton
      {
        focusPolicy: Qt.NoFocus
        action: boldAction
        display: AbstractButton.IconOnly
      }
      ToolButton
      {
        focusPolicy: Qt.NoFocus
        action: italicAction
        display: AbstractButton.IconOnly
      }
      ToolButton
      {
        focusPolicy: Qt.NoFocus
        action: underlineAction
        display: AbstractButton.IconOnly
      }
      ToolSeparator {
      }
      ToolButton
      {
        focusPolicy: Qt.NoFocus
        action: alignLeftAction
        display: AbstractButton.IconOnly
      }
      ToolButton
      {
        focusPolicy: Qt.NoFocus
        action: alignCenterAction
        display: AbstractButton.IconOnly
      }
      ToolButton
      {
        focusPolicy: Qt.NoFocus
        action: alignRightAction
        display: AbstractButton.IconOnly
      }
      ToolButton
      {
        focusPolicy: Qt.NoFocus
        action: alignJustifyAction
        display: AbstractButton.IconOnly
      }
      ToolSeparator {
      }
      
    }
  }
  TextArea
  {
    id: textArea
    property variant node
    property variant editData
    property string lastText: node.latestData(BDB.Data.Text).asText()
    onNodeChanged: editData = null
    text: lastText
    textFormat: TextEdit.RichText
    placeholderText: "Enter text..."
    selectByMouse: true
    wrapMode: TextEdit.WordWrap
    onTextChanged: {
      if(textArea.node && text != lastText)
      {
        if(textArea.editData == null)
        {
          textArea.editData = textArea.node.newDataPoint(BDB.Data.Text)
        }
        textArea.editData.setText(text)
        textArea.editData.record()
      }
    }
    DocumentHandler
    {
      id: document
      textDocument: textArea.textDocument
      cursorPosition: textArea.cursorPosition
      selectionStart: textArea.selectionStart
      selectionEnd: textArea.selectionEnd
    }
  }
}
