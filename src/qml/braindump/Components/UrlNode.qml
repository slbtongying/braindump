import QtQuick 2.0
import QtQuick.Controls 2.0

import braindump.db 1.0 as BDB

Row
{
  id: root
  property variant node
  property variant editData
  property variant lastUrl: node.latestData(BDB.Data.Url).asObject()
  onNodeChanged: editData = null
  function __updateUrl()
  {
    if(root.node)
    {
      if(root.editData == null)
      {
        root.editData = root.node.newDataPoint(BDB.Data.Url)
      }
      var obj = { "label": labelEditor.text, "url": urlEditor.text }
      root.editData.setObject(obj)
      root.editData.record()
    }
  }
  TextField
  {
    id: labelEditor
    text: root.lastUrl["label"]
    placeholderText: "Enter label..."
    onTextChanged: if(root.lastUrl["label"] != labelEditor.text) root.__updateUrl()
  }
  Label
  {
    text: ":"
  }
  TextField
  {
    id: urlEditor
    text: root.lastUrl["url"]
    placeholderText: "Enter url..."
    onTextChanged: if(root.lastUrl["url"] != urlEditor.text && root.node) root.__updateUrl()
  }
  Button
  {
    text: "open"
    onPressed: {
      Qt.openUrlExternally(urlEditor.text)
    }
  }
}
