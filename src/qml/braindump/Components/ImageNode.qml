import QtQuick 2.0
import braindump.items 1.0
import QtQuick.Controls 2.0
import QtQuick.Dialogs 1.2

import braindump.db 1.0 as BDB

Row
{
  id: root
  property variant node
  ImageDataItem
  {
    id: dataItem
    data: node.latestData(BDB.Data.Image)
    width: imageWidth
    height: imageHeight
    
    
    
  }
  Button
  {
    text: "open"
    FileDialog
    {
      id: openDialog
      nameFilters: [ "Image files (*.jpg *.png *.tiff)" ]
      onAccepted: {
        var dp = root.node.newDataPoint(BDB.Data.Image)
        if(dataItem.loadImage(dp, openDialog.fileUrl))
        {
          dp.record()
          dataItem.data = Qt.binding(function() { return node.latestData() })
        } else {
          errorDialog.open()
        }
      }
    }
    MessageDialog
    {
      id: errorDialog
      icon: StandardIcon.Critical
      standardButtons: MessageDialog.Ok
      text: "Failed to load image"
    }
    onClicked: openDialog.open()
  }
}
