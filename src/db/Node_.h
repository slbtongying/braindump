public:
  Q_INVOKABLE QList<QObject*> parentNodes() const;
  Q_INVOKABLE QList<QObject*> childNodes() const;
  Q_INVOKABLE QObject* createNewChild(int _type);
  Q_INVOKABLE QObject* latestData(braindump::db::Data::Type _type) const;
  Q_INVOKABLE QObject* newDataPoint(braindump::db::Data::Type _type) const;
