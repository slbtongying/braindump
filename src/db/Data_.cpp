#include "db/Data_p.h"

#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>

using namespace braindump::db;

QString Data::asText() const
{
  return QString::fromUtf8(data());
}

void Data::setText(const QString& _text)
{
  setData(_text.toUtf8());
}

QVariantMap Data::asObject() const
{
  QJsonParseError err;
  QJsonDocument doc = QJsonDocument::fromJson(data(), &err);
  if(doc.isNull())
  {
    qWarning() << "Invalid JSON document: " << err.errorString();
    return QVariantMap();
  } else {
    return doc.object().toVariantMap();
  }
}

void Data::setObject(const QVariantMap& _map)
{
  QJsonObject object = QJsonObject::fromVariantMap(_map);
  QJsonDocument doc(object);
  setData(doc.toJson(QJsonDocument::Compact));
}
