#include "ImageDataItem.h"

#include <QQuickWindow>
#include <QSGSimpleTextureNode>

#include <db/Data.h>

using namespace braindump::items;

struct ImageDataItem::Private
{
  braindump::db::Data* data = nullptr;
  QImage m_image;
  QSGTexture* m_texture;
  bool refresh_texture;
};

ImageDataItem::ImageDataItem(QQuickItem *_parent) : d(new Private)
{
  setFlag(QQuickItem::ItemHasContents);
  d->m_texture = nullptr;
}

ImageDataItem::~ImageDataItem()
{
  delete d->m_texture;
  delete d;
}

braindump::db::Data * ImageDataItem::data() const
{
  return d->data;
}

void ImageDataItem::setData(const braindump::db::Data* _data)
{
  if(d->data)
  {
    d->data->deleteLater();
  }
  d->data = new braindump::db::Data(*_data);
  d->data->setParent(this);
  d->m_image = QImage::fromData(d->data->data());
  d->refresh_texture = true;
  emit(dataChanged());
  update();
}

/*
QImage ImageDataItem::image() const
{
  return d->m_image;
}

void ImageDataItem::setImage(const QImage& _image)
{
  d->m_image = _image;
  d->refresh_texture = true;
  emit(imageChanged());
  update();
}*/

QSGNode* ImageDataItem::updatePaintNode(QSGNode *_oldNode, UpdatePaintNodeData *_upnd)
{

  if(d->refresh_texture)
  {
    delete d->m_texture;
    d->m_texture = window()->createTextureFromImage(d->m_image);

    QSGSimpleTextureNode* textureNode = new QSGSimpleTextureNode;
    textureNode->setRect(0, 0, width(), height());
    textureNode->setTexture(d->m_texture);

    delete _oldNode;
    return textureNode;
  } else {
    return _oldNode;
  }
}

qreal ImageDataItem::imageHeight() const
{
  return d->m_image.height();
}

qreal ImageDataItem::imageWidth() const
{
  return d->m_image.width();
}

bool ImageDataItem::loadImage(braindump::db::Data* _data, const QUrl& _url)
{
  QFile file(_url.toLocalFile());
  if(file.open(QIODevice::ReadOnly))
  {
    _data->setData(file.readAll());
    return true;
  } else {
    return false;
  }
}

#include "moc_ImageDataItem.cpp"

