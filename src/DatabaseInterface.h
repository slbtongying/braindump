#ifndef INTERFACE_H
#define INTERFACE_H

#include <QObject>
#include <QMutex>

#include "db/Node.h"

namespace QtSqlite3 {
  class Database;
}

namespace braindump
{
  class DatabaseInterface : public QObject
  {
    Q_OBJECT
    Q_PROPERTY(QObject* root READ rootAsQObject CONSTANT);
  public:
    explicit DatabaseInterface(QObject *parent = nullptr);
    static void setDatabaseFilename(const QString& _filename);
  private:
    QObject* rootAsQObject() { return &m_root; }
  private:
    db::Database* m_database;
    QtSqlite3::Database* m_sqlite_database;
    db::Node m_root;
    static QString s_database_filename;
  };
}

#endif // INTERFACE_H

