#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QIcon>

#include "DatabaseInterface.h"
#include "DocumentHandler.h"
#include "db/Data.h"
#include "db/Node.h"
#include "items/ImageDataItem.h"

int main(int argc, char *argv[])
{
  qRegisterMetaType<braindump::db::Node::Type>("::braindump::db::Node::Type");
  qRegisterMetaType<braindump::db::Data::Type>("::braindump::db::Data::Type");
  qRegisterMetaType<braindump::db::Data*>("::braindump::db::Data*");
  
  QGuiApplication app(argc, argv);
  QGuiApplication::setApplicationName("braindump");
  QGuiApplication::setApplicationVersion("1.9.0");

  // If no theme is set, default to breeze
  if(QIcon::themeName().isEmpty())
  {
    QIcon::setThemeName("breeze");
  }

  const char* uri = "braindump";
  qmlRegisterType<braindump::DatabaseInterface>(uri, 1, 0, "DatabaseInterface");
  qmlRegisterType<braindump::DocumentHandler>(uri, 1, 0, "DocumentHandler");

  const char* uri_db = "braindump.db";
  qmlRegisterUncreatableType<braindump::db::Data>(uri_db, 1, 0, "Data", "");
  qmlRegisterUncreatableType<braindump::db::Node>(uri_db, 1, 0, "Node", "");

  const char* uri_items = "braindump.items";
  qmlRegisterType<braindump::items::ImageDataItem>(uri_items, 1, 0, "ImageDataItem");
  
  QQmlApplicationEngine engine;
  engine.addImportPath("qrc:/qml");
  engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));
  return app.exec();
}
