# - Find PARC
# Find the Parc includes, library and tool
# This module defines
#  PARC_INCLUDE_DIR, where to find parc/AbstractCodeGenerator.h
#  PARC_LIBRARIES, the libraries needed to use parc.
#  PARC_PROGRAM path to the parc executablex
#  PARC_FOUND, If false, parc was not found


# Copyright (c) 2015, Cyrille Berger, <cyrille.berger@liu.se>
#
# Redistribution and use is allowed according to the terms of the BSD license.
# For details see the accompanying COPYING-CMAKE-SCRIPTS file.


# use pkg-config to get the directories and then use these values
# in the FIND_PATH() and FIND_LIBRARY() calls

find_path(PARC_INCLUDE_DIR parc/AbstractCodeGenerator.h
   PATHS
   ${PARC_DIR}/include
   PATH_SUFFIXES parc
)

find_library(PARC_LIBRARIES NAMES parc
   PATHS
   ${PARC_DIR}/lib
   PATH_SUFFIXES parc
)

find_program(PARC_PROGRAM NAMES parc
  PATHS
  ${PARC_DIR}/bin
  PATH_SUFFIXES parc
)

if(PARC_INCLUDE_DIR AND PARC_LIBRARIES)
   set(PARC_FOUND TRUE)
else(PARC_INCLUDE_DIR AND PARC_LIBRARIES)
   set(PARC_FOUND FALSE)
endif(PARC_INCLUDE_DIR AND PARC_LIBRARIES)

if(NOT PARC_FOUND)
   if(NOT PARC_FIND_QUIETLY)
      if(PARC_FIND_REQUIRED)
         message(FATAL_ERROR "Required package PARC NOT found")
      else(PARC_FIND_REQUIRED)
         message(STATUS "PARC NOT found")
      endif(PARC_FIND_REQUIRED)
   endif(NOT PARC_FIND_QUIETLY)
endif(NOT PARC_FOUND)

mark_as_advanced(PARC_INCLUDE_DIR PARC_LIBRARIES PARC_PROGRAM PARC_VERSION)

